package com.itstyle.seckill;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 鍚姩绫�
 * 鍒涘缓鑰� 绉戝府缃�
 * 鍒涘缓鏃堕棿	2018骞�5鏈�12鏃�
 * API鎺ュ彛娴嬭瘯锛歨ttp://localhost:8080/seckill/swagger-ui.html
 * 璺戜箣鍓� 涓�瀹氳鐪嬫枃搴擄細https://gitee.com/52itstyle/spring-boot-seckill/wikis
 */
@SpringBootApplication
public class Application {
	private final static Logger LOGGER = LoggerFactory.getLogger(Application.class);
	/**
	 * 1. 鏁版嵁搴撲箰瑙傞攣锛�2. 鍩轰簬Redis鐨勫垎甯冨紡閿侊紱3. 鍩轰簬ZooKeeper鐨勫垎甯冨紡閿�
	 * 4. redis 璁㈤槄鐩戝惉锛�5.kafka娑堟伅闃熷垪
	 * 鍚姩鍓� 璇烽厤缃產pplication.properties涓浉鍏硆edis銆亃k浠ュ強kafka鐩稿叧鍦板潃
	 */
	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(Application.class, args);
		LOGGER.info("椤圭洰鍚姩 ");
	}
}